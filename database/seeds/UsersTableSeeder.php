<?php
Use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  public function run()
  { 
  	factory(App\User::class, 100)->create(); 
	//Creamos los usuarios iniciales
  	DB::table('users')->insert([
            'name' => 'Nancy Leon Hernandez',
            'email' => 'esperanza123@gmail.com',
            'password' => '$2y$10$t7ZE/dKHDNSSvS7VcWxz0ukFnhn10kXxsrScLYtLhwxMxcmquEMfe',
        ]);

        DB::table('users')->insert([
            'name' => 'Beatriz Rodriguez Ribon',
            'email' => 'besa@gmail.com',
            'password' => '$2y$10$t7ZE/dKHDNSSvS7VcWxz0ukFnhn10kXxsrScLYtLhwxMxcmquEMfe',
        ]);
  }
}
