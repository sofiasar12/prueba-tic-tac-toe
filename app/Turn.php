<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Turn extends Model
{
  //iniciamos variables de la base de datos
  public $incrementing = false;
  protected $guarded = [];
  // protected $primaryKey = ['id', 'game_id'];

  public function game() 
  { 
  	return $this->belongsTo(Game::class, 'game_id'); 
	}
}
