<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;
  //iniciamos variables de la base de datos
  protected $fillable = ['name', 'email', 'password', 'score'];

  protected $hidden = ['password', 'remember_token'];
}
